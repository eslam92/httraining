#!/bin/bash
#AUTHOR=Eslam Adel<eslam.adel.2006@gmail.com>
#PURPOSE=testing bash scripts
NAME="Eslam"
WHERE="HorizonTech"
FILES=$(cat /home/eslamadel/horizion/credintials.txt)
W=`who`
clear                           #clear terminal window

echo Hello  $NAME
echo i am in $WHERE atm
echo
echo $NAME  testing 1
echo $NAME 2 testing 2
echo $NAME 3 testing 3
echo
echo "script name is $0"
echo "my location is $PWD"
echo  the date  is `date`
echo
echo these users are currently connected
echo $W
echo $PATH
echo this the the bash version $BASH_VERSION
echo
#command sub example
echo $FILES
echo 2+2
echo 2+2 |bc
echo 120/3 |bc
echo 7*7 |bc
echo 
echo "thats all folks"