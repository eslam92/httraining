#!/bin/bash
#author=eslam adel <eslam.adel@horizontechs.com>
#purpose= checking if port 80 is open on domain


DOMAINS=$(cat $1 | cut -f1 -d:)
nc2() {
        for DOMAIN in $DOMAINS
        do
                nc -z -v -w5 $DOMAIN 80 2>&1 | grep -q "Ncat: Connected to"
            if [ $? == 0 ]
            then
                echo "$DOMAIN OK!"
            else
                echo "$DOMAIN FAILED!"
            fi
        done
}

#Calling Function
nc2
