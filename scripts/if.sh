#true case

if [ 200 > 120 ]
then
        echo "Right"
else
        echo "wrong"
fi

if [ 200 -lt 120 ]
then
        echo "yep"
else
        echo "nah"      
fi

#false case

if [ 300 -gt 500 ]
then
        echo "oki"
else
        echo "nah"
fi

if [ 300 = 500 ]
then
        echo "good"
else
        echo "bad"
fi
