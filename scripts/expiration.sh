#author=eslam adel <eslam.adel@horizontechs.com>
#purpose=getting expiration date for domains from a file

File=$1

WhoIs() {


        for Line in $(cat $1  | cut -f1 -d\:)

        do echo -n  "$Line Expiry date is ->"
whois $Line | grep "Registry Expiry Date" | cut -f2 -d\:
        done
}

#Calling Function
WhoIs $File

#whois $File | grep "Registry Expiry Date" | cut -f2 -d\:
